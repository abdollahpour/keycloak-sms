# WPIC Keycloak SMS
​
## Technologies:
This project will require editing and reimplementing JBoss Keycloak code. This will involve:
- Reimplementing Keycloak Auth SPI in Java.
- Altering the login template to support the new functionality.
​
## Task Description:
We are using Keycloak as our primary authentication method in all in-house applications.
​
Currently, Keycloak does not include functionality for phone number authentication (using your phone number as a unique identifier in place of a username or email address).
​
This functionality is required on the Chinese market. Chinese users expect this functionality and as such are deterred by lengthy forms or other authentication methods. This can cause our clients to lose many users.
​
Your task is to add implement the following functionality:
1. Phone number authentication.
2. SMS token verification.
3. An updated UI template to support these functions.
​
### Phone number authentication:
To add a new authentication, you must reimplement Keycloak Auth SPI (See [here](http://keycloak.github.io/docs/userguide/keycloak-server/html/auth_spi.html)).
​
Users should be asked to register using only a phone number and password - no username, email, etc...
​
SMS login exists in Keycloak already, but only as 2nd stage security after a conventional username/email login. You need to implement this as the first (and only) authentication factor.
​
### SMS token verification:
When a user registers their phone number, they will need to verify the new account. This will be done via SMS token (the user should receive a verification code via SMS).
​
This first login is known a 'first broker login' in the Keycloak lexicon. More information on this can be found [here](http://keycloak.github.io/docs/userguide/keycloak-server/html/auth_spi.html#d4e3906).
​
You will need to add a second 'Required Action' to the authentication flow for first broker logins only.
​
### Template:
You will need to update the Keycloak login template to support these changes.
​
This involves either altering the existing Keycloak SPI templates or reimplementing your own from scratch. Either approach is acceptable, choose whichever you prefer.
​
The flow of the application should be as follows:
1. User enters phone number.
2. User receives verification code via SMS.
3. User enters verification code.
4. Logged in.
​
Avoid adding any additional stages as this will deter Chinese users. For an example of a similar login system, look into WeChat.
​
## Additional Constraints:
You should complete this task using the most common standard technologies for each stage:
- Reimplement the Keycloak SPI in Java.
- Use standard HTML for the updated templates.
- etc...
​
This task will involve researching how to do this for yourself. Our team are not familiar with this process and this will be an independent task. Do not accept this task if you are not confident you can complete it.
​
***Please contact Hamed Abdollahpour (CTO) with any further questions***
***(email: hamed@web-presence-in-china.com)***
